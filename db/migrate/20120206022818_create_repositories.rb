class CreateRepositories < ActiveRecord::Migration
  def change
    create_table :repositories do |t|
      t.string :name
      t.string :homepage
      t.string :git_url
      t.string :description
      t.string :repository_url
      t.integer :user_id
      t.text :readme
      t.integer :language_id
      t.timestamps
    end
  end
end