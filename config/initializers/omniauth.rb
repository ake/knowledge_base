Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, FACEBOOK_KEY, FACEBOOK_SECRET, :scope => 'email,offline_access,read_stream,publish_stream,user_groups', :display => 'popup'
end