rails_env = ENV['RAILS_ENV'] || 'production'

# 16 workers and 1 master
worker_processes (rails_env == 'production' ? 1 : 1)

# Load rails+github.git into the master before forking workers
# for super-fast worker spawn times
preload_app true

# Restart any workers that haven't responded in 30 seconds
timeout 30

# Listen on a Unix data socket
listen 7000

RAIL_ROOT = '/home/dgm59/git/knowledge_base'
stderr_path RAIL_ROOT+'/tmp/std_err'
stdout_path RAIL_ROOT+'/tmp/std_out'

after_fork do |server, worker|
  ActiveRecord::Base.establish_connection
end

before_fork do |server, worker|
  old_pid = RAIL_ROOT + '/tmp/pids/unicorn.pid.oldbin'
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
    end
  end
end
