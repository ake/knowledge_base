require_relative "bootstrap_helper.rb"

module ApplicationHelper
  def flash_class(level)
    case level
    when :notice then 'info'
    when :error then 'error'
    when :alert then 'warning'
    end
  end
  # helper_method :current_user

  def markdown(text)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML,
            :autolink => true, :space_after_headers => true)
    markdown.render(text).html_safe
  end
  
  
  def first_x_words(string, n=20,finish='...')
     string.split(' ')[0,n].inject{|sum,word| sum + ' ' + word} + finish
  end
  
  # BOOTSTRAP HELPERS
  
  
  include BootstrapHelper
  
  
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  
  
end
