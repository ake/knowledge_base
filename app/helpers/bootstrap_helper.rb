module BootstrapHelper
  def icon(icon_name)
    raw("<i class='icon-#{icon_name}'></i>"+ " ")
  end
  
  def pre(code,extension = nil)
    raw("<pre class='prettyprint preview lang-#{extension}'>#{code}</pre>").html_safe
  end
  
  def tagging(tag_name, type="default")
    raw("<span class='label label-#{type}'>#{tag_name}</span>").html_safe
  end
end