class ApplicationDecorator < Draper::Base
  # Lazy Helpers
  #   PRO: Call Rails helpers without the h. proxy
  #        ex: number_to_currency(model.price)
  #   CON: Add a bazillion methods into your decorator's namespace
  #        and probably sacrifice performance/memory
  #
  #   Enable them by uncommenting this line:
  #   lazy_helpers

  # Shared Decorations
  #   Consider defining shared methods common to all your models.
  #
  #   Example: standardize the formatting of timestamps
  #
  #   def formatted_timestamp(time)
  #     h.content_tag :span, time.strftime("%a %m/%d/%y"),
  #                   :class => 'timestamp'
  #   end
  #
  #   def created_at
  #     formatted_timestamp(model.created_at)
  #   end
  #
  #   def updated_at
  #     formatted_timestamp(model.updated_at)
  #   end
  
  def updated_by
    h.icon("user") + h.time_ago_in_words(model.updated_at) + " ago by " + h.link_to(model.user)
  end

  
  
  def delete_btn
    if can_modify
      h.link_to (h.icon("remove") + "delete"), model, :method=>:delete, :confirm => "Are you sure?", :class=>"btn"
    end
  end
  
  
  private 
  
  def can_modify
    model.user == h.current_user || h.current_user.try(:is_admin)
  end
  
end