class RepositoriesController < InheritedResources::Base
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'
  def create
    @repository = Repository.new(params[:repository])
    if @repository.save
      redirect_to root_path
    else
      redirect_to root_path, :flash => {:error => @repository.errors}
    end
  end
  
  def destroy  
    destroy! { root_path }  
  end
end