class OverviewController < ApplicationController
  def index
    @repositories = RepositoryDecorator.decorate Repository.order("updated_at DESC")
    @snippets = SnippetDecorator.decorate Snippet.order("updated_at DESC")
    @links = LinkDecorator.decorate Link.order("updated_at DESC")
    @pages = PageDecorator.decorate Page.order("updated_at DESC")
    
    @link = Link.new
    @repository = Repository.new
    @snippet = Snippet.new
    @page = Page.new
  end
  
  
  
  
  def details
    
  end
end
