class LinksController < InheritedResources::Base
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'
  def create
    @link = Link.new(params[:link])
    if @link.save
      redirect_to root_path
    else
      redirect_to root_path, :flash => {:error => @link.errors}
    end
  end
  def destroy  
    destroy! { root_path }  
  end
end
