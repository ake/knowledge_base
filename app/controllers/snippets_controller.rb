class SnippetsController < InheritedResources::Base
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'
  def create  
    create! { root_path }  
  end
  
  def destroy  
    destroy! { root_path }  
  end
end
