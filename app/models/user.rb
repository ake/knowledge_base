class User < ActiveRecord::Base
  has_many :snippets
  has_many :repositories
  has_many :links
  has_many :pages
  
  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.name = auth["info"]["name"]
    end
  end
  
  def update_info(auth)
    self.auth_token = auth[:credentials][:token]
    self.nickname = auth[:info][:nickname]
    self.image = auth[:info][:image]
    self.save
  end
  
  
  def contribution
    personal = 0
    
    [Repository, Snippet].each do |klass|
      personal += klass.count(:conditions => "user_id = #{self.id}")
    end
    return personal
  end
  
  def to_s
    name
  end
end