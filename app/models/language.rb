class Language < ActiveRecord::Base
  has_many :snippets
  has_many :repositories
  
  def name
    self.attributes["name"].capitalize
  end
  
  def to_s
    self.name
  end
  
end
