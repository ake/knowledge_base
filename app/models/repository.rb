class Repository < ActiveRecord::Base
  acts_as_taggable
  before_validation :fetch_info
  belongs_to :user
  belongs_to :language
  validates :repository_url, :uniqueness => true, :presence => true
  validates :name, :presence => true
  
  
  def fetch_info
    self.repository_url = self.repository_url.gsub(".git", "").strip.gsub(/\#.*$/, "").gsub(/\/$/, "")

    # GITHUB
    if self.repository_url.match(/^https\:\/\/github\.com\/[a-z0-9A-Z_-]+\/[\.a-z0-9A-Z_-]+$/)
      begin
        github_repo = GitHub::Repository.new self.repository_url
        [:name, :git_url, :homepage, :description].each do |meth|
          self.send("#{meth}=".to_sym, github_repo.send(meth))
        end
        self.language = Language.find_by_name(github_repo.language.downcase)
      rescue GitHub::APIRequestError => e
        errors.add(:github_request_error, e.message)
      end
    else
      errors.add(:repository_url, "\"#{self.repository_url}\" Doesn't match Github Repository URL format")
    end
  end

  def homepage
    self.attributes["homepage"].blank? ?  self.repository_url : self.attributes["homepage"]
  end

  def primary_tag
    self.language.name
  end

end
