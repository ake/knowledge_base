class Snippet < ActiveRecord::Base
  belongs_to :language
  belongs_to :user
  acts_as_taggable
  validates :name, :uniqueness => true, :presence => true
  validates :body, :uniqueness => true, :presence => true
  validates :description, :uniqueness => true, :presence => true
  validates :language, :presence => true
  
  def preview(lines = 100)
    body.split("\n")[0,lines].join("\n")
  end
  
  def primary_tag
    language.name
  end
end