namespace :fixture do
  task :create_languages => :environment do
    languages = %w{javascript,js
    ruby,rb
    python,py	
    shell,sh
    java,java
    php,php
    c,c
    perl,pl	
    c++,cpp
    objective-c,m
    css,css
    coffeescript,coffee
    }.map{|x| x.split(",")}
    languages.each do |lang|
      Language.find_or_create_by_name_and_extension(lang.first.downcase.strip, lang.last.downcase.strip)
      puts "added #{lang}"
    end
  end
  
  task :create_tags => :environment do
    tags = %w{cloud deployment database api xml json rails documentation
      ajax coffeescript screencast video linux ubuntu mac vim xcode
      android css css3 html5 jquery bash book github graph gem ipad
      iphone library networking mobile monitoring mongodb mysql template tip
      }
    tags.each do |tag|
      Tag.find_or_create_by_name(tag.downcase.strip)
      puts "added #{tag}"
    end
  end
  
  task :all do
    Rake::Task["fixture:create_tags"].invoke
    Rake::Task["fixture:create_languages"].invoke
  end
  
end